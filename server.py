#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa Servidor UDP ( SIP )
"""
import json
import socketserver
import sys
import threading
import time

# Constantes
# Puerto
PORT = 6001


def json2registered():
    """
        Método que busca un historial de usuarios
    """
    try:
        with open("registered.json", encoding="utf-8", newline="\n") as r:
            for line in r:
                line_dict = json.loads(line)
                key = list(json.loads(line).keys())[0]
                SIPRegisterHandler.store[key] = line_dict[key]
        print("Inicializando el Servidor. Se ha encontrado historial de usuarios")
    except Exception:
        print("Inicializando el Servidor. No se ha encontrado historial de usuarios")


def write_in_file(content):
    """
       Método de escritura en fichero
       :param content: Contenido para escribir
    """
    with open("registered.json", "a", newline="\n") as r:
        r.write(str(content).replace("'", '"'))
        r.write("\n")


def remove_by_expire(address_registry: str, address: str, expire: int):
    """
       Método que gestiona la caducidad de los usuarios registrados
       :param address_registry: dirección de registro
       :param address: dirección
    """
    SIPRegisterHandler.store.pop(address_registry, None)
    expires_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
    content = {address_registry: {"address": address, "expires": expires_time}}
    write_in_file(content)
    print("SIP/2.0 200 OK\n")


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
       Clase SIPRegisterHandler
    """
    # atributo de la clase
    store = {}

    def handle(self):
        """
            Método handle del servidor
            (todas las peticiones serán registradas por este método)
         """
        self.wfile.write("Registro Recibido\n".encode('utf-8'))
        client_address = self.client_address[0]
        msg = self.request[0].decode('utf-8')
        address_registry = msg.rstrip().split(" ")[1].split(":")[1]
        expire_value = int(msg.rstrip().split(" ")[5])
        ip = client_address
        self.store[address_registry] = ip
        start_time = threading.Timer(
            float(expire_value),
            remove_by_expire,
            kwargs={"address_registry": address_registry, "address": client_address, "expire": expire_value}
        )
        start_time.start()


def main():
    try:
        json2registered()
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Servidor escuchando por el puerto: {PORT}\n")
        serv.serve_forever()
    except OSError as e:
        sys.exit(f"Error al escuchar: {e.args[1]}\n")
    except KeyboardInterrupt:
        print("Servidor Finalizado\n")
        sys.exit(0)


if __name__ == "__main__":
    main()

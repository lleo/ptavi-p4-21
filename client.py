#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa Cliente UDP ( SIP )

Ejemplo comando: python client.py localhost 6001 register luke@polismassa.com 3600
"""

import socket
import sys


def main():
    """
        Método principal:
            - creación del socket
            - configuración
            - vínculo servidor/puerto
    """
    try:
        SERVER = sys.argv[1]
        PORT = int(sys.argv[2])
        LINE = sys.argv[3:]

        REGISTER_COMMAND = [line.upper() for line in LINE if line == "register"][0]
        LINE = [line for line in LINE if line != "register"]
        ADDRESS_REGISTRY = LINE[0]
        EXPIRES = int(LINE[1])

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            LINE = REGISTER_COMMAND + " sip:" + ADDRESS_REGISTRY + f" SIP/2.0 \r\n Expires: {EXPIRES}\r\n\r\n"
            print(f"Enviando a {SERVER}:{PORT}:",
                  REGISTER_COMMAND + " " + ADDRESS_REGISTRY + f" SIP/2.0 \r\n Expires: {EXPIRES}\r\n\r\n")
            my_socket.send(LINE.encode('utf-8') + b'\r\n')
        print("Cliente terminado.")
    except Exception:
        print("Usage: client.py ip puerto register sip_address expires_value")


if __name__ == "__main__":
    main()
